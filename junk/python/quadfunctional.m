%% COMPUTE RELATIVE ERROR FUNCTIONAL

function [func] = quadfunctional(u, beta0, beta1, R, fp, gp, hp, fp2, gp2, hp2)

d = u(1);
%  kappa = u(2);
kappa = 0;
psi = u(3);
theta = u(4);

% COMPUTE P AND Q
if(abs(kappa) > 1e-10)
  p = sqrt(2/kappa^2*(-(1+d*kappa)+sqrt((1+d*kappa)^2 + kappa^2*(R^2-d^2))));
else  
  p = sqrt(R^2-d^2);
end
q = d + 0.5*kappa*p^2;

% COMPUTE ZEROTH, FIRST, AND SECOND MOMENTS IN PRIME COORDINATE SYSTEM
V = 2*pi * (-0.5*d*p^2 + 1/3*R^3 - 1/3*(R^2-p^2)^1.5 - 1/8*p^4*kappa);
Ixxr = pi/30 * (-15*d*p^4 + sqrt(R^2-p^2)*(12*p^4 - 4*p^2*R^2-8*R^4) ...
          - 5*p^6*kappa + 8*R^5);
Iyyr = pi/480 * (-160*d^3*p^2 + 128*R^5 - sqrt(R^2 - p^2) * (128*R^4 - 96*p^2*R^2 - 32*p^4) - 120*d^2*p^4*kappa - 40*p^6*kappa - 5*p^8*kappa^3 - 40*d*p^4*(3 + p^2*kappa^2));
       
% CONSIDER CASE OF Q < 0
if (q < 0)
  V = V + 4*pi/3*(R^2-p^2)^1.5;
  Ixxr = Ixxr + 4*pi/15*(R^2-p^2)^1.5 * (3*p^2 + 2*R^2);
  Iyyr = Iyyr + 2*pi/15*(R^2-p^2)^1.5 * (p^2 + 4*R^2);
end

Ixr = pi/4 *(2*p^2*(R^2-d^2) - 1/6*p^6*kappa^2 - p^4*(1+d*kappa));
Izzr = Iyyr;

% COMPUTE ROTATION QUANTITIES
cp = cos(psi);
sp = sin(psi);
ct = cos(theta);
st = sin(theta);

% STORE VOLUME
f = V;

% ROTATE FIRST MOMENTS INTO UNPRIMED COORDINATE SYSTEM
g = [cp*ct*Ixr; sp*ct*Ixr; -st*Ixr]; 

% ROTATE SECOND MOMENTS INTO UNPRIMED COORDINATE SYSTEM
%  h = rot*[Ixxr 0 0; 0 Iyyr 0; 0 0 Izzr]*rot';
h = [Iyyr*sp^2 + cp^2*(ct^2*Ixxr + Izzr*st^2), cp*sp*(ct^2*Ixxr - Iyyr + Izzr*st^2), cp*ct*(-Ixxr + Izzr)*st;
        cp*sp*(ct^2*Ixxr - Iyyr + Izzr*st^2), cp^2*Iyyr + sp^2*(ct^2*Ixxr + Izzr*st^2), ct*(-Ixxr + Izzr)*sp*st;
        cp*ct*(-Ixxr + Izzr)*st, ct*(-Ixxr + Izzr)*sp*st, ct^2*Izzr + Ixxr*st^2];

% COMPUTE RELATIVE ERROR FUNCTIONAL
e0 = (f - fp)^2 / fp2;
dg = g - gp;
e1 = (dg(1)^2 + dg(2)^2 + dg(3)^2) / gp2;
dh = h - hp;
e2 = (dh(1,1)^2 + dh(2,2)^2 + dh(3,3)^2 + 2*(dh(1,2)^2 + dh(1,3)^2 + dh(2,3)^2)) / hp2;

func = beta0*e0 + beta1*e1 + (1-beta0-beta1)*e2;
