#include <stdlib.h>
#include <random>
#include <functional>   // std::bind
#include <algorithm>    // std::generate_n
#include <iomanip>      // std::setprecision
#include <iostream>
#include <string>
#include <vector>

using namespace std;

int main() {
	vector<double> v(380/2*380/2*370/2);
	
	auto step_size   = 500ul;
	auto total_steps = v.size() / step_size;

	size_t steps_completed = 0;
	cout << "Progress: 0.0%\n";

	#pragma omp parallel 
	{
		size_t local_count = 0;

		#pragma omp parallel for
		for(size_t i = 0; i < v.size(); i++){
			v[i] = 1.;
			
			if (local_count++ % step_size == step_size-1){
				#pragma omp atomic
            ++steps_completed;

            if (steps_completed % 100 == 0){
					#pragma omp critical
					
					cout << "Progress: " << fixed << setprecision(1) << (100.0*steps_completed/total_steps) << "%\n";
				}
			}
		}
	}
	cout << "Done" << endl;

	return 0;
}