#include <iostream>
#include <iomanip>
#include <stdlib.h>
#include <fstream>
#include <sstream>
#include <string.h>
#include <vector>
#include <teem/nrrd.h>
#include <omp.h>

using namespace std;

int main(int argc, char **argv) 
{
	// DECIPHER COMMAND LINE ARGUMENTS
	if(argc < 2){
		cerr << "ERROR" << endl;
		cerr << "Please provide a NRRD file" << endl;
		cerr << "For example: ./vsitegen sphere.nrrd ptcloud.ply 1.5" << endl;
		exit(1);
	}
	if(argc < 3){
		cerr << "ERROR" << endl;
		cerr << "Please provide a point cloud PLY file" << endl;
		cerr << "For example: ./vsitegen sphere.nrrd ptcloud.ply 1.5" << endl;
		exit(1);
	}
	if(argc < 4){
		cerr << "ERROR" << endl;
		cerr << "Please provide Voronoi site distance" << endl;
		cerr << "For example: ./vsitegen sphere.nrrd ptcloud.ply 1.5" << endl;
		exit(1);
	}
	
	printf("-------------------------------------------------------------------------------\n");
	printf("Image mask: %s\n", argv[1]);
	printf("Voronoi site dist parameter: %g voxels\n", atof(argv[3]));
	printf("-------------------------------------------------------------------------------\n");	
	
	// READ IMAGE
	char me[]="demoIO", *err;
	Nrrd *nin;
	double d[3], dxp;

	nin = nrrdNew();
	if (nrrdLoad(nin, argv[1], NULL)) {
		err = biffGetDone(NRRD);
		fprintf(stderr, "%s: trouble reading \"%s\":\n%s", me, argv[1], err);
		free(err);
		exit(1);
	}
	for (int i = 0; i < 3; i++){
		d[i] = nin->axis[i].spacing;
	}
	dxp = d[0];
	nrrdNuke(nin);
	
	// READ POINT CLOUD
	string dummy;
	int numpoints;
	ifstream fid;
	fid.open(argv[2]);
	
	getline(fid, dummy);
	getline(fid, dummy);
	fid >> dummy >> dummy >> numpoints >> dummy;
	for (int i = 0; i < 9; i++){
		getline(fid, dummy);
	}
	
	vector<double> loc(numpoints*3);
	vector<double> normal(numpoints*3);
	
	for (int i = 0; i < numpoints; i++){
		fid >> loc[3*i + 0] >> loc[3*i + 1] >> loc[3*i + 2];
		fid >> normal[3*i + 0] >> normal[3*i + 1] >> normal[3*i + 2];
	}
	fid.close();
	
	// EXPORT VORONOI SITES
	double sitepairdist = atof(argv[3]) * dxp;
	int numsites = 2 * numpoints;
	vector<double> sites(numsites*3);
	
	printf("Number of Voronoi sites: %d\n", numsites);

	for (int i = 0; i < numpoints; i++){
		sites[3*i + 0] = loc[3*i + 0] - sitepairdist * normal[3*i + 0];
		sites[3*i + 1] = loc[3*i + 1] - sitepairdist * normal[3*i + 1];
		sites[3*i + 2] = loc[3*i + 2] - sitepairdist * normal[3*i + 2];
	}
	for (int i = 0; i < numpoints; i++){
		sites[3*(numpoints + i) + 0] = loc[3*i + 0] + sitepairdist * normal[3*i + 0];
		sites[3*(numpoints + i) + 1] = loc[3*i + 1] + sitepairdist * normal[3*i + 1];
		sites[3*(numpoints + i) + 2] = loc[3*i + 2] + sitepairdist * normal[3*i + 2];
	}
	
	ofstream fileID; // open a file in write mode
	fileID.open("vor.xyz"); 
	fileID << "3 " << numsites << endl;
	
	for (int i = 0; i < numsites; i++){
		fileID << setprecision(6) << scientific;
		fileID << sites[3*i + 0] << " " << sites[3*i + 1] << " " << sites[3*i + 2] << endl;
	}

	fileID.close();

	return 0;
}
